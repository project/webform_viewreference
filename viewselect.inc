<?php

/**
 * @file
 * Webform module multiple select component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_viewselect() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'mandatory' => 0,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(
      'items' => '',
      'multiple' => NULL,
      'aslist' => NULL,
      'optrand' => 0,
      'title_display' => 0,
      'description' => '',
      'custom_keys' => FALSE,
      'options_source' => '',
      'private' => FALSE,
      'disabled' => '0',
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_viewselect() {
  return array(
    'webform_display_viewselect' => array(
      'arguments' => array('element' => NULL),
      'file' => 'viewselect.inc',
    ),
    'webform_viewselect_option_text' => array(
        'arguments' => array('rendered' => FALSE, 'row' => NULL, 'field' => NULL),
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_viewselect($component) {
  $form = array();

  $views = array('--' => '--');
  $all_views = views_get_all_views();
  foreach ($all_views as $view) {
    // Only 'node' views that have fields will work for our purpose.
    $allowed_types = array('node', 'term_data', 'users');
    if (in_array($view->base_table, $allowed_types) && !empty($view->display['default']->display_options['fields'])) {
      if ($view->type == 'Default') {
        $views[t('Default Views')][$view->name] = $view->name;
      }
      else {
        $views[t('Existing Views')][$view->name] = $view->name;
      }
    }
  }
  

  $form['extra']['advanced'] = array(
     '#type' => 'fieldset',
     '#title' => t('Advanced - Nodes that can be referenced (View)'),
     '#weight' => -100,
   );
   
  if (count($views) > 1) {
    $form['extra']['advanced']['advanced_view'] = array(
      '#type' => 'select',
      '#title' => t('View used to select the nodes'),
      '#options' => $views,
      '#required' => TRUE,
      '#default_value' => isset($component['extra']['advanced']['advanced_view']) ? $component['extra']['advanced']['advanced_view'] : '--',
      '#description' => t('<p>Choose the "Views module" view that selects the nodes that can be referenced.<br />Note:</p>') .
        t('<ul><li>Only views that have fields will work for this purpose.</li><li>Use the view\'s "sort criteria" section to determine the order in which candidate nodes will be displayed.</li></ul>'),
    );
    $form['extra']['advanced']['advanced_view_args'] = array(
      '#type' => 'textfield',
      '#title' => t('View arguments'),
      '#default_value' => isset($component['extra']['advanced']['advanced_view_args']) ? $component['extra']['advanced']['advanced_view_args'] : '',
      '#required' => FALSE,
      '#description' => t('Provide a comma separated list of arguments to pass to the view.'),
    );
    
   $form['extra']['advanced']['advanced_rendered'] = array(
      '#type' => 'radios',
      '#title' => t('Display text'),
      '#default_value' => isset($component['extra']['advanced']['advanced_rendered']) ? $component['extra']['advanced']['advanced_rendered'] : '0',
      '#required' => FALSE,
      '#options' => array('0' => t('Node title'), '1' => t('Rendered view field (title)')),
      '#description' => t('Choose which text that should be shown in the form to the end-user. Rendered view field only show title field, bu you can use field rewrite to populate with other data.'),
    );
  }
  
   $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field. For multiple selects use commas to separate multiple defaults.') . theme('webform_token_help'),
    '#size' => 60,
    '#maxlength' => 256,
    '#weight' => 0,
  );
  
  $form['extra']['multiple'] = array(
    '#type' => 'checkbox',
    '#title' => t('Multiple'),
    '#return_value' => 'Y',
    '#default_value' => $component['extra']['multiple'],
    '#description' => t('Check this option if the user should be allowed to choose multiple values.'),
  );
  
  $form['extra']['aslist'] = array(
    '#type' => 'checkbox',
    '#title' => t('Listbox'),
    '#return_value' => 'Y',
    '#default_value' => $component['extra']['aslist'],
    '#description' => t('Check this option if you want the select component to be of listbox type instead of radiobuttons or checkboxes.'),
  );
  
  $form['display']['disabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disabled'),
      '#return_value' => 1,
      '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
      '#weight' => 11,
      '#default_value' => $component['extra']['disabled'],
      '#parents' => array('extra', 'disabled'),
  );
  
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_viewselect($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#theme_wrappers' => array('webform_element_wrapper'),
    '#pre_render' => array('webform_element_title_display'),
    '#post_render' => array('webform_element_wrapper'),
    '#translatable' => array('title', 'description', 'options'),
  );

  // Convert the user-entered options list into an array.
  $default_value = $filter ? _webform_filter_values($component['value'], $node, NULL, NULL, FALSE) : $component['value'];

  $args = $component['extra']['advanced']['advanced_view_args'];
  $rendered = $component['extra']['advanced']['advanced_rendered'];
  $options = _webform_viewselect_options($component['extra']['advanced']['advanced_view'], $args, FALSE, $rendered);
  
  if ($component['extra']['optrand']) {
    _webform_shuffle_options($options);
  }

  // Add default options if using a select list with no default. This matches
  // Drupal 7's handling of select lists.
  if ($component['extra']['aslist'] && !$component['extra']['multiple'] && $default_value === '') {
    $options = array('' => ($component['mandatory'] ? t('- Select -') : t('- None -'))) + $options;
  }

  // Set the component options.
  $element['#options'] = $options;

  // Set the default value.
  if (isset($value)) {
    if ($component['extra']['multiple']) {
      // Set the value as an array.
      $element['#default_value'] = array();
      foreach ((array) $value as $key => $option_value) {
        $element['#default_value'][] = $option_value;
      }
    }
    else {
      // Set the value as a single string.
      $element['#default_value'] = '';
      foreach ((array) $value as $option_value) {
        $element['#default_value'] = $option_value;
      }
    }
  }
  elseif ($default_value !== '') {
    // Convert default value to a list if necessary.
    if ($component['extra']['multiple']) {
      $varray = explode(',', $default_value);
      foreach ($varray as $key => $v) {
        $v = trim($v);
        if ($v !== '') {
          $element['#default_value'][] = $v;
        }
      }
    }
    else {
      $element['#default_value'] = $default_value;
    }
  }
  elseif ($component['extra']['multiple']) {
    $element['#default_value'] = array();
  }

  if ($component['extra']['other_option'] && module_exists('select_or_other')) {
    // Set display as a select_or_other element:
    $element['#type'] = 'select_or_other';
    $element['#other'] = !empty($component['extra']['other_text']) ? check_plain($component['extra']['other_text']) : t('Other...');
    $element['#other_unknown_defaults'] = 'other';
    $element['#other_delimiter'] = ', ';
    $element['#process'] = array('select_or_other_process', 'webform_expand_viewselect_or_other');
    if ($component['extra']['multiple']) {
      $element['#multiple'] = TRUE;
      $element['#select_type'] = 'checkboxes';
    }
    else {
      $element['#multiple'] = FALSE;
      $element['#select_type'] = 'radios';
    }
    if ($component['extra']['aslist']) {
      $element['#select_type'] = 'select';
    }
  }
  elseif ($component['extra']['aslist']) {
    // Set display as a select list:
    $element['#type'] = 'select';
    if ($component['extra']['multiple']) {
      $element['#size'] = 4;
      $element['#multiple'] = TRUE;
    }
  }
  else {
    if ($component['extra']['multiple']) {
      // Set display as a checkbox set.
      $element['#type'] = 'checkboxes';
      // Drupal 6 hack to properly render on multipage forms.
      $element['#process'] = array('webform_expand_viewselect_checkboxes');
      $element['#pre_render'][] = 'webform_expand_viewselect_ids';
    }
    else {
      // Set display as a radio set.
      $element['#type'] = 'radios';
      $element['#pre_render'][] = 'webform_expand_viewselect_ids';
    }
  }

  if ($component['extra']['disabled']) {
    // Make the component readonly if user is not in submission edit mode
    if (arg(2) != 'submission' && arg(4) != 'edit') {
      $element['#attributes']['disabled'] = 'disabled';
    }
  }
  
  return $element;
}

/**
 * Process function to ensure select_or_other elements validate properly.
 */
function webform_expand_viewselect_or_other($element) {
  // Disable validation for back-button and save draft.
  $element['select']['#validated'] = TRUE;
  $element['select']['#webform_validated'] = FALSE;

  $element['other']['#validated'] = TRUE;
  $element['other']['#webform_validated'] = FALSE;

  // The Drupal FAPI does not support #title_display inline so we need to move
  // to a supported value here to be compatible with select_or_other.
  $element['select']['#title_display'] = $element['#title_display'] === 'inline' ? 'before' : $element['#title_display'];

  // If the default value contains "select_or_other" (the key of the select
  // element for the "other..." choice), discard it and set the "other" value.
  if (is_array($element['#default_value']) && in_array('select_or_other', $element['#default_value'])) {
    $key = array_search('select_or_other', $element['#default_value']);
    unset($element['#default_value'][$key]);
    $element['#default_value'] = array_values($element['#default_value']);
    $element['other']['#default_value'] = implode(', ', $element['#default_value']);
  }

  // Sanitize the options in Select or other check boxes and radio buttons.
  if ($element['#select_type'] == 'checkboxes') {
    $element['select']['#pre_render'][] = 'webform_expand_viewselect_ids';
  }
  elseif ($element['#select_type'] == 'radios') {
    $element['select']['#pre_render'][] = 'webform_expand_viewselect_ids';
  }

  return $element;
}


/**
 * FAPI process function to rename IDs attached to checkboxes and radios.
 */
function webform_expand_viewselect_ids($element) {
  $id = $element['#id'] = str_replace('_', '-', _webform_safe_name(strip_tags($element['#id'])));
  $delta = 0;
  foreach (element_children($element) as $key) {
    $delta++;
    // Convert the #id for each child to a safe name, regardless of key.
    $element[$key]['#id'] = $id . '-' . $delta;

    // Prevent scripts or CSS in the labels for each checkbox or radio.
    $element[$key]['#title'] = _webform_filter_xss($element[$key]['#title']);
  }
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_viewselect($component, $value, $format = 'html') {
  $args = $component['extra']['advanced']['advanced_view_args'];
  $rendered = $component['extra']['advanced']['advanced_rendered'];
  $options = _webform_viewselect_options($component['extra']['advanced']['advanced_view'], $args, FALSE, $rendered);

  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_viewselect',
    '#theme_wrappers' => $format == 'html' ? array('webform_element', 'webform_element_wrapper') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#format' => $format,
    '#options' => $options,
    '#value' => (array) $value,
    '#translatable' => array('title', 'options'),
  );
}

/**
 * Implements _webform_submit_component().
 *
 * Convert FAPI 0/1 values into something saveable.
 */
function _webform_submit_viewselect($component, $value) {
  // Build a list of all valid keys expected to be submitted.
  $options = _webform_viewselect_options($component['extra']['advanced']['advanced_view'], $component['extra']['advanced']['advanced_view_arguments'], TRUE, $rendered);

  $return = NULL;
  if (is_array($value)) {
    $return = array();
    foreach ($value as $key => $option_value) {
      // Handle options that are specified options.
      if ($option_value !== '' && isset($options[$option_value])) {
        // Checkboxes submit a value of FALSE when unchecked. A checkbox with
        // a value of '0' is valid, so we can't use empty() here.
        if ($option_value === FALSE && !$component['extra']['aslist'] && $component['extra']['multiple']) {
          unset($value[$option_value]);
        }
        else {
          $return[] = $option_value;
        }
      }
      // Handle options that are added through the "other" field. Specifically
      // exclude the "select_or_other" value, which is added by the select list.
      elseif ($component['extra']['other_option'] && module_exists('select_or_other') && $option_value != 'select_or_other') {
        $return[] = $option_value;
      }
    }
  }
  elseif (is_string($value)) {
    $return = $value;
  }

  return $return;
}

/**
 * Format the text output for this component.
 */
function theme_webform_display_viewselect($element) {
  $component = $element['#webform_component'];

  // Flatten the list of options so we can get values easily. These options
  // may be translated by hook_webform_display_component_alter().
  $options = array();
  foreach ($element['#options'] as $key => $value) {
    if (is_array($value)) {
      foreach ($value as $subkey => $subvalue) {
        $options[$subkey] = $subvalue;
      }
    }
    else {
      $options[$key] = $value;
    }
  }

  $items = array();
  if ($component['extra']['multiple']) {
    foreach ((array) $element['#value'] as $option_value) {
      if ($option_value !== '') {
        // Administer provided values.
        if (isset($options[$option_value])) {
          $items[] = $element['#format'] == 'html' ? _webform_filter_xss($options[$option_value]) : $options[$option_value];
        }
        // User-specified in the "other" field.
        else {
          $items[] = $element['#format'] == 'html' ? check_plain($option_value) : $option_value;
        }
      }
    }
  }
  else {
    if (isset($element['#value'][0]) && $element['#value'][0] !== '') {
      // Administer provided values.
      if (isset($options[$element['#value'][0]])) {
        $items[] = $element['#format'] == 'html' ? _webform_filter_xss($options[$element['#value'][0]]) : $options[$element['#value'][0]];
      }
      // User-specified in the "other" field.
      else {
        $items[] = $element['#format'] == 'html' ? check_plain($element['#value'][0]) : $element['#value'][0];
      }
    }
  }

  if ($element['#format'] == 'html') {
    $output = count($items) > 1 ? theme('item_list', $items) : (isset($items[0]) ? $items[0] : ' ');
  }
  else {
    if (count($items) > 1) {
      foreach ($items as $key => $item) {
        $items[$key] = ' - ' . $item;
      }
      $output = implode("\n", $items);
    }
    else {
      $output = isset($items[0]) ? $items[0] : ' ';
    }
  }

  return $output;
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_viewselect($component, $sids = array(), $single = FALSE) {
  $args = $component['extra']['advanced']['advanced_view_args'];
  $rendered = $component['extra']['advanced']['advanced_rendered'];
  $options = _webform_viewselect_options($component['extra']['advanced']['advanced_view'], $args, FALSE, $rendered);
  $show_other_results = $single;

  $sid_placeholders = count($sids) ? array_fill(0, count($sids), "'%s'") : array();
  $sid_filter = count($sids) ? " AND sid IN (" . implode(",", $sid_placeholders) . ")" : "";

  $option_operator = $show_other_results ? 'NOT IN' : 'IN';
  $placeholders = count($options) ? array_fill(0, count($options), "'%s'") : array();
  $query = 'SELECT data, count(data) as datacount ' .
    ' FROM {webform_submitted_data} ' .
    ' WHERE nid = %d ' .
    ' AND cid = %d ' .
    " AND data != ''" . $sid_filter .
    ($placeholders ? ' AND data ' . $option_operator . ' (' . implode(',', $placeholders) . ')' : '') .
    ' GROUP BY data ';

  $count_query = 'SELECT count(*) as datacount ' .
    ' FROM {webform_submitted_data} ' .
    ' WHERE nid = %d ' .
    ' AND cid = %d ' .
    " AND data != ''" . $sid_filter;

  $result = db_query($query, array_merge(array($component['nid'], $component['cid']), $sids, array_keys($options)));
  $rows = array();
  $normal_count = 0;
  while ($data = db_fetch_array($result)) {
    $display_option = $single ? $data['data'] : $options[$data['data']];
    $rows[$data['data']] = array(_webform_filter_xss($display_option), $data['datacount']);
    $normal_count += $data['datacount'];
  }

  if (!$show_other_results) {
    // Order the results according to the normal options array.
    $ordered_rows = array();
    foreach (array_intersect_key($options, $rows) as $key => $label) {
      $ordered_rows[] = $rows[$key];
    }

    // Add a row for any unknown or user-entered values.
    if ($component['extra']['other_option']) {
      $full_count = db_result(db_query($count_query, array_merge(array($component['nid'], $component['cid']), $sids)));
      $other_count = $full_count - $normal_count;
      $display_option = !empty($component['extra']['other_text']) ? check_plain($component['extra']['other_text']) : t('Other...');
      $other_text = $other_count ? $other_count . ' (' . l(t('view'), 'node/' . $component['nid'] . '/webform-results/analysis/' . $component['cid']) . ')' : $other_count;
      $ordered_rows[] = array($display_option, $other_text);
    }

    $rows = $ordered_rows;
  }

  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_viewselect($component, $value) {
  // Convert submitted 'safe' values to un-edited, original form.
  $args = $component['extra']['advanced']['advanced_view_args'];
  $rendered = $component['extra']['advanced']['advanced_rendered'];
  $options = _webform_viewselect_options($component['extra']['advanced']['advanced_view'], $args, FALSE, $rendered);

  $value = (array) $value;
  $items = array();
  // Set the value as a single string.
  foreach ($value as $option_value) {
    if ($option_value !== '') {
      if (isset($options[$option_value])) {
        $items[] = _webform_filter_xss($options[$option_value]);
      }
      else {
        $items[] = check_plain($option_value);
      }
    }
  }

  return implode('<br />', $items);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_viewselect($component, $export_options) {
  $headers = array(
    0 => array(),
    1 => array(),
    2 => array(),
  );

  if ($component['extra']['multiple'] && $export_options['select_format'] == 'separate') {
    $headers[0][] = '';
    $headers[1][] = $component['name'];
    $args = $component['extra']['advanced']['advanced_view_args'];
    $rendered = $component['extra']['advanced']['advanced_rendered'];
    $items = _webform_viewselect_options($component['extra']['advanced']['advanced_view'], $args, FALSE, $rendered);
    if ($component['extra']['other_option']) {
      $other_label = !empty($component['extra']['other_text']) ? check_plain($component['extra']['other_text']) : t('Other...');
      $items[$other_label] = $other_label;
    }
    $count = 0;
    foreach ($items as $key => $item) {
      // Empty column per sub-field in main header.
      if ($count != 0) {
        $headers[0][] = '';
        $headers[1][] = '';
      }
      if ($export_options['select_keys']) {
        $headers[2][] = $key;
      }
      else {
        $headers[2][] = $item;
      }
      $count++;
    }
  }
  else {
    $headers[0][] = '';
    $headers[1][] = '';
    $headers[2][] = $component['name'];
  }
  return $headers;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_viewselect($component, $export_options, $value) {
  $args = $component['extra']['advanced']['advanced_view_args'];
  $rendered = $component['extra']['advanced']['advanced_rendered'];
  $options = _webform_viewselect_options($component['extra']['advanced']['advanced_view'], $args, FALSE, $rendered);
  $return = array();

  if ($component['extra']['multiple']) {
    foreach ($options as $key => $item) {
      $index = array_search($key, (array) $value);
      if ($index !== FALSE) {
        if ($export_options['select_format'] == 'separate') {
          $return[] = 'X';
        }
        else {
          $return[] = $export_options['select_keys'] ? $key : $item;
        }
        unset($value[$index]);
      }
      elseif ($export_options['select_format'] == 'separate') {
        $return[] = '';
      }
    }

    // Any remaining items in the $value array will be user-added options.
    if ($component['extra']['other_option']) {
      $return[] = count($value) ? implode(',', $value) : '';
    }
  }
  else {
    $key = $value[0];
    if ($export_options['select_keys']) {
      $return = $key;
    }
    else {
      $return = isset($options[$key]) ? $options[$key] : $key;
    }
  }

  if ($component['extra']['multiple'] && $export_options['select_format'] == 'compact') {
    $return = implode(',', (array) $return);
  }

  return $return;
}

/**
 * Menu callback; Return a predefined list of select options as JSON.
 */
function webform_viewselect_options_ajax($source_name = '') {
  $info = _webform_viewselect_options_info();

  $component['extra']['options_source'] = $source_name;
  if ($source_name && isset($info[$source_name])) {
    $options = _webform_viewselect_options_to_text(_webform_viewselect_options($component, !$component['extra']['aslist'], FALSE));
  }
  else {
    $options = '';
  }

  $return = array(
    'elementId' => module_exists('options_element') ? 'edit-items-options-options-field-widget' : 'edit-extra-items',
    'options' => $options,
  );

  drupal_json($return);
}

/**
 * Generate a list of options for a select list.
 */
function _webform_viewselect_options($view, $args = '', $strict = TRUE, $rendered = FALSE) {
  $options = array();

  // Prepare field array - Reusing CCK nodreference function _nodereference_potential_references()
  $field['advanced_view'] = $view;
  $field['advanced_view_args'] = $args;
  // Field name used for cid in _nodereference_potential_references
  $field['field_name'] = md5($view . $args); 
  
  $data = _webform_nodereference_potential_references($field);

  foreach ($data as $index => $row) { 
    $options[$index] = theme('webform_viewselect_option_text', $rendered, $row, $field); 
  }

  return $options;
}

/**
 * Load Webform select option info from 3rd party modules.
 */
function _webform_viewselect_options_info() {
  static $info;
  if (!isset($info)) {
    $info = array();

    foreach (module_implements('webform_viewselect_options_info') as $module) {
      $additions = module_invoke($module, 'webform_viewselect_options_info');
      foreach ($additions as $key => $addition) {
        $additions[$key]['module'] = $module;
      }
      $info = array_merge($info, $additions);
    }
    drupal_alter('webform_viewselect_options_info', $info);
  }
  return $info;
}

/**
 * Execute a select option callback.
 *
 * @param $name
 *   The name of the options group.
 * @param $component
 *   The full Webform component.
 * @param $flat
 *   Whether the information returned should exclude any nested groups.
 * @param $filter
 *   Whether information returned should be sanitized. Defaults to TRUE.
 */
function _webform_viewselect_options_callback($name, $component, $flat = FALSE, $filter = TRUE) {
  $info = _webform_viewselect_options_info();

  // Include any necessary files.
  if (isset($info[$name]['file'])) {
    $pathinfo = pathinfo($info[$name]['file']);
    $path = ($pathinfo['dirname'] ? $pathinfo['dirname'] . '/' : '') . basename($pathinfo['basename'], '.' . $pathinfo['extension']);
    module_load_include($pathinfo['extension'], $info[$name]['module'], $path);
  }

  // Execute the callback function.
  if (isset($info[$name]['options callback']) && function_exists($info[$name]['options callback'])) {
    $function = $info[$name]['options callback'];

    $arguments = array();
    if (isset($info[$name]['options arguments'])) {
      $arguments = $info[$name]['options arguments'];
    }

    return $function($component, $flat, $filter, $arguments);
  }
}

/**
 * Utility function to split user-entered values from new-line seperated
 * text into an array of options.
 *
 * @param $text
 *   Text to be converted into a select option array.
 * @param $flat
 *   Optional. If specified, return the option array and exclude any optgroups.
 * @param $filter
 *   Optional. Whether or not to filter returned values.
 */
function _webform_viewselect_options_from_text($text, $flat = FALSE, $filter = TRUE) {
  static $option_cache = array();

  // Keep each processed option block in an array indexed by the MD5 hash of
  // the option text and the value of the $flat variable.
  $md5 = md5($text);

  // Check if this option block has been previously processed.
  if (!isset($option_cache[$flat][$md5])) {
    $options = array();
    $rows = array_filter(explode("\n", trim($text)));
    $group = NULL;
    foreach ($rows as $option) {
      $option = trim($option);
      /**
       * If the Key of the option is within < >, treat as an optgroup
       *
       * <Group 1>
       *   creates an optgroup with the label "Group 1"
       *
       * <>
       *   Unsets the current group, allowing items to be inserted at the root element.
       */
      if (preg_match('/^\<([^>]*)\>$/', $option, $matches)) {
        if (empty($matches[1])) {
          unset($group);
        }
        elseif (!$flat) {
          $group = $filter ? _webform_filter_values($matches[1], NULL, NULL, NULL, FALSE) : $matches[1];
        }
      }
      elseif (preg_match('/^([^|]+)\|(.*)$/', $option, $matches)) {
        $key = $filter ? _webform_filter_values($matches[1], NULL, NULL, NULL, FALSE) : $matches[1];
        $value = $filter ? _webform_filter_values($matches[2], NULL, NULL, NULL, FALSE) : $matches[2];
        isset($group) ? $options[$group][$key] = $value : $options[$key] = $value;
      }
      else {
        $filtered_option = $filter ? _webform_filter_values($option, NULL, NULL, NULL, FALSE) : $option;
        isset($group) ? $options[$group][$filtered_option] = $filtered_option : $options[$filtered_option] = $filtered_option;
      }
    }

    $option_cache[$flat][$md5] = $options;
  }

  // Return our options from the option_cache array.
  return $option_cache[$flat][$md5];
}

/**
 * Convert an array of options into text.
 */
function _webform_viewselect_options_to_text($options) {
  $output = '';
  $previous_key = FALSE;

  foreach ($options as $key => $value) {
    // Convert groups.
    if (is_array($value)) {
      $output .= '<' . $key . '>' . "\n";
      foreach ($value as $subkey => $subvalue) {
        $output .= $subkey . '|' . $subvalue . "\n";
      }
      $previous_key = $key;
    }
    // Typical key|value pairs.
    else {
      // Exit out of any groups.
      if (isset($options[$previous_key]) && is_array($options[$previous_key])) {
        $output .= "<>\n";
      }
      // Skip empty rows.
      if ($options[$key] !== '') {
        $output .= $key . '|' . $value . "\n";
      }
      $previous_key = $key;
    }
  }

  return $output;
}

function theme_webform_viewselect_option_text($rendered, $row, $field) {
  if ($rendered) {
    return html_entity_decode(_webform_filter_xss($row['rendered']));
  } 
  else {
    return html_entity_decode(_webform_filter_xss($row['title']));
  }
}

/**
 * Drupal 6 hack that properly *renders* checkboxes in multistep forms. This is
 * different than the value hack needed in Drupal 5, which is no longer needed.
 */
function webform_expand_viewselect_checkboxes($element) {
  // Elements that have a value set are already in the form structure cause
  // them not to be written when the expand_checkboxes function is called.
  $default_value = array();
  foreach (element_children($element) as $key) {
    if (isset($element[$key]['#default_value'])) {
      $default_value[$key] = $element[$key]['#default_value'];
      unset($element[$key]);
    }
  }

  $element = expand_checkboxes($element);

  // Escape the values of checkboxes.
  foreach (element_children($element) as $key) {
    $element[$key]['#return_value'] = check_plain($element[$key]['#return_value']);
    $element[$key]['#name'] = $element['#name'] . '[' . $element[$key]['#return_value'] . ']';
    $element[$key]['#value_callback'] = 'webform_checkbox_value';
    $element[$key]['#pre_render'][] = 'webform_checkbox_prerender';
  }

  foreach ($default_value as $key => $val) {
    $element[$key]['#default_value'] = $val;
  }
  return $element;
}
