
-- SUMMARY --

This module adds a webform component where you could use a View as a datasource for your normal webform select component. Instead of hardcoding all the selectable values in the component values field you just select a reference to a View similar to the CCK nodereference. The selectlist / radiobuttons /checkboxes then automatically gets filled with all the nodes from the View.

This module works only with Webform 6.3x

The modules is based on Webform Select Component and CCK Nodreference. 